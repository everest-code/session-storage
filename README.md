# Universal Session Storage
An universal front-end and back-end modifiable session.

Version: `SessionStorage/2.0`

## Why use `USS`?
USS or Universal Session Storage is a fast and easy method to share and manage the users session with the purpose to use it as an alternative to cookies, also works to manage the session from the frontend.

## JS Library
This library still in development.

Coming soon you can use it :)

## Getting Started and First setup
USS use the flags to set options instead of a configuration file, the principal options are the connection to **Redis**, **TTL** or Time to live of a session and the expose options.

### `--help` output
```bash
$ session-storage --help

usage: session-storage [-h|--help] [-p|--expose "<value>"] [-v|--verbosity
                       (DEBG|INFO|WARN|ERRO)] [-r|--redis-host "<value>"]
                       [-t|--ttl "<value>"] [-d|--redis-db <integer>]
                       [-w|--redis-password "<value>"]

                       Universal & simple session handler with redis

Arguments:

  -h  --help            Print help information
  -p  --expose          [host]:port to expose the database. Default: :9000
  -v  --verbosity       Log level for debugging and useful data. Default: WARN
  -r  --redis-host      host:port to connect the database. Default:
                        localhost:6379
  -t  --ttl             time to live for a session. Default: 8h
  -d  --redis-db        db to connect the database. Default: 0
  -w  --redis-password  password to connect the database
```

### All flags
- `-h | --help`: Print help information
- `-p | --expose`: address to expose the database (application).
    - Pattern: **`[host]:port`**
    - Default value: **`:9000`**
- `-v | --verbosity`: Log level for debugging and useful data.
    - Select: **`DEBG|INFO|WARN|ERRO`**
    - Default value: **`WARN`**
- `-r | --redis-host`: addres of redis database.
    - Pattern: **`host:port`**
    - Default value: **`localhost:6379`**
- `-t | --ttl`: session duration or (TTL) of a user/session.
    - Default value: **`8h`**
- `-d | --redis-db`: DB to connect the database.
    - Default value: **`0`**
- `-w | --redis-password`: Password of redis database authentication.

### Simple example
```bash
# Expose app to: 0.0.0.0:9000
# Connect to redis: localhost:6379
# Set a TTL of: 2 hours
session-storage -p "0.0.0.0:9000" -v INFO -r "localhost:6379" -t 2h
```

## API Docs
See the API documentation [here](https://everest-code.gitlab.io/session-storage/index.html).

## Donations
[![Suport this project!!!](https://github.com/aha999/DonateButtons/raw/master/Paypal.png)](https://www.paypal.com/donate?hosted_button_id=TJPCWB5ABTVXA)
