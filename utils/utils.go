package utils

import (
	"encoding/json"
	"errors"
	"io/ioutil"
	"net/http"
	"net/url"
	"strings"
)

var IsNotJSONError error = errors.New("The body is not a JSON")

func BindJson(r *http.Request, obj interface{}) error {
	contentType := strings.Trim(strings.Split(r.Header.Get("Content-Type"), ";")[0], " ")
	if contentType != "application/json" {
		return IsNotJSONError
	}

	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		return err
	}

	return json.Unmarshal(body, &obj)
}

func AsJson(obj interface{}) (string, error) {
	body, err := json.Marshal(obj)
	if err != nil {
		return "", err
	}

	return string(body), nil
}

func FromJson(rawBody string, obj interface{}) error {
	return json.Unmarshal([]byte(rawBody), obj)
}

func WriteJson(w http.ResponseWriter, obj interface{}) error {
	body, err := json.MarshalIndent(obj, "", "  ")
	if err != nil {
		return err
	}

	w.Header().Add("Content-Type", "application/json; charset=utf-8")

	_, err = w.Write(body)
	return err
}

func ValueToJson(obj interface{}) (string, error) {
	body, err := json.Marshal(obj)
	if err != nil {
		return "", err
	}

	return string(body), nil
}

func ReadUserIP(r *http.Request) (IPAddress string) {
	IPAddress = r.Header.Get("X-Real-Ip")

	if IPAddress == "" {
		IPAddress = r.Header.Get("X-Forwarded-For")
	}

	if IPAddress == "" {
		url, _ := url.Parse("http://" + r.RemoteAddr)
		IPAddress = url.Hostname()
	}

	return
}
