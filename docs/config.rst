=====================
 Configuration Server
=====================

**Tree-DB** doesn't use a configuration file else use flags to configure a change easily the behaviour of the server.

Flags
-----
.. sourcecode:: sh

    usage: session-storage [-h|--help] [-p|--expose "<value>"] [-v|--verbosity
                           (DEBG|INFO|WARN|ERRO)] [-r|--redis-host "<value>"]
                           [-t|--ttl "<value>"] [-d|--redis-db <integer>]
                           [-w|--redis-password "<value>"]

                           Universal & simple session handler with redis

    Arguments:

      -h  --help            Print help information
      -p  --expose          [host]:port to expose the database. Default: :9000
      -v  --verbosity       Log level for debugging and useful data. Default: WARN
      -r  --redis-host      host:port to connect the database. Default:
                            localhost:6379
      -t  --ttl             time to live for a session. Default: 8h
      -d  --redis-db        db to connect the database. Default: 0
      -w  --redis-password  password to connect the database


- ``-h | --help``: Print help information.

- ``-p | --expose``: address to expose the database (application).
    - Pattern: **[host]:port**
    - Default value: **:9000**

- ``-v | --verbosity``: Log level for debugging and useful data.
    - Select: **DEBG|INFO|WARN|ERRO**
    - Default value: **WARN**

- ``-r | --redis-host``: addres of redis database.
    - Pattern: **host:port**
    - Default value: **localhost:6379**

- ``-t | --ttl``: session duration or (TTL) of a user/session.
    - Default value: **8h**

- ``-d | --redis-db``: DB to connect the database.
    - Default value: **0**
    
- ``-w | --redis-password``: Password of redis database authentication.
