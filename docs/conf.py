# -*- coding: utf-8 -*-

source_suffix = ".rst"
master_doc = "index"

# General information about the project.
project = u"Universal Session Storage"
copyright = u"2022, Everest Code"
author = u"Sebastian Gaviria"
version = u"1.0.0"

language = "en"
pygments_style = "sphinx"
html_theme = "sphinx_rtd_theme"

rst_epilog = """
.. include:: substitutions.txt
"""
