package main

import (
	"net/http"

	"github.com/gorilla/mux"
	"gitlab.com/everest-code/session-storage/core"
	"gitlab.com/everest-code/session-storage/db"
	"gitlab.com/everest-code/tree-db/server/log"
)

var Logger *log.Logger

func main() {
	args := parseArgs()
	Logger, _ = log.NewConsole(args.GetLogLevel(), false)

	ttl, err := args.GetTTL()
	if err != nil {
		Logger.Panic(err.Error())
	}

	router := mux.NewRouter()

	rdb, err := db.CreateConnection(
		*args.RedisHost,
		*args.RedisDB,
		*args.RedisPassword,
		Logger,
	)
	if err != nil {
		Logger.Panic(err.Error())
	} else {
		Logger.Debug("Server connected to RedisDB")
	}

	sessionHandler := core.NewSessionHandler(
		rdb,
		Logger,
		ttl,
	)

	router.StrictSlash(false)
	router.Use(loggerMiddleware)
	router.Use(headerMiddleware)
	router.PathPrefix("/").HandlerFunc(core.EmptyHandler).Methods("OPTIONS")

	router.HandleFunc("/__info__", core.IndexHandler)

	router.HandleFunc("/session", sessionHandler.CreateSession).Methods("POST")
	router.HandleFunc("/session/id", sessionHandler.GetSessionId).Methods("POST")
	router.HandleFunc("/session/ttl_extend", sessionHandler.TtlExtendSession).Methods("PUT")
	router.HandleFunc("/session", sessionHandler.DeleteSession).Methods("DELETE")
	router.HandleFunc("/metadata", sessionHandler.SessionMetadata).Methods("GET")
	router.HandleFunc("/storage", sessionHandler.GetAllsession).Methods("GET")
	router.HandleFunc("/storage", sessionHandler.SetSessionValue).Methods("POST")
	router.HandleFunc("/storage/__multi__", sessionHandler.SetSessionValues).Methods("POST")
	router.HandleFunc("/storage/{key:[a-zA-Z_0-9]+}", sessionHandler.GetAValue).Methods("GET")
	router.HandleFunc("/storage/{key:[a-zA-Z_0-9]+}", sessionHandler.DeleteValue).Methods("DELETE")

	Logger.ImportantInfo("Server listening on %s", *args.Expose)
	if err := http.ListenAndServe(*args.Expose, router); err != nil {
		Logger.Panic(err.Error())
	}
}
