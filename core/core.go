package core

import (
	"errors"
	"net/http"
	"strings"
	"time"

	"github.com/google/uuid"
	"github.com/gorilla/mux"
	"gitlab.com/everest-code/session-storage/db"
	"gitlab.com/everest-code/session-storage/utils"
	"gitlab.com/everest-code/tree-db/server/log"
)

type (
	SessionHandler struct {
		Client     *db.SessionDatabase
		Logger     *log.Logger
		TtlDefault time.Duration
	}
)

func IndexHandler(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusOK)
	utils.WriteJson(w, NewResponse("SessionStorage/2.0", nil))
}

func EmptyHandler(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusNoContent)
}

func NewSessionHandler(client *db.SessionDatabase, logger *log.Logger, ttl time.Duration) *SessionHandler {
	return &SessionHandler{
		Client:     client,
		Logger:     logger,
		TtlDefault: ttl,
	}
}

func (sh *SessionHandler) raiseDBError(w http.ResponseWriter, err error) {
	if errors.Is(err, db.UserNotFound) {
		w.WriteHeader(http.StatusNotFound)
		utils.WriteJson(w, NewErrorResponse(&ApiError{
			Name:        "NotFound",
			Description: err.Error(),
		}))
		return
	}

	sh.Logger.Error("Error on database: %s", err.Error())
	w.WriteHeader(http.StatusInternalServerError)
	utils.WriteJson(w, NewErrorResponse(&ApiError{
		Name:        "DBError",
		Description: err.Error(),
	}))
}

func (sh *SessionHandler) raiseJSONError(w http.ResponseWriter, err error) {
	sh.Logger.Debug("Error on JSON serialization: %s", err.Error())
	w.WriteHeader(http.StatusBadRequest)
	utils.WriteJson(w, NewErrorResponse(&ApiError{
		Name:        "DecodeError",
		Description: err.Error(),
	}))
}

func (sh *SessionHandler) raiseNotFound(w http.ResponseWriter) {
	w.WriteHeader(http.StatusNotFound)
	utils.WriteJson(w, NewErrorResponse(&ApiError{
		Name:        "SessionError",
		Description: "Session ID was not found",
	}))
}

func (sh *SessionHandler) CreateSession(w http.ResponseWriter, r *http.Request) {
	user := 0

	if err := utils.BindJson(r, &user); err != nil {
		sh.raiseJSONError(w, err)
		return
	}

	id, err := sh.Client.CreateEmpty(user, sh.TtlDefault)
	if err != nil {
		if errors.Is(err, db.AlreadyExistsUser) {
			w.WriteHeader(http.StatusBadRequest)
			utils.WriteJson(w, NewErrorResponse(&ApiError{
				Name:        "SessionError",
				Description: err.Error(),
			}))
			return
		}
		sh.raiseDBError(w, err)
		return
	}

	_ = sh.Client.SetValue(id, "_user_agent", r.Header.Get("User-Agent"))
	_ = sh.Client.SetValue(id, "_ip", utils.ReadUserIP(r))

	w.WriteHeader(http.StatusCreated)
	utils.WriteJson(w, NewResponse(id, nil))
}

func (sh *SessionHandler) GetSessionId(w http.ResponseWriter, r *http.Request) {
	user := 0

	if err := utils.BindJson(r, &user); err != nil {
		sh.raiseJSONError(w, err)
		return
	}

	id, err := sh.Client.GetUserID(user)
	if err != nil {
		if errors.Is(err, db.UserNotFound) {
			w.WriteHeader(http.StatusNotFound)
			utils.WriteJson(w, NewErrorResponse(&ApiError{
				Name:        "SessionError",
				Description: err.Error(),
			}))
			return
		}
		sh.raiseDBError(w, err)
		return
	}

	utils.WriteJson(w, NewResponse(id, nil))
}

func (sh *SessionHandler) SessionMetadata(w http.ResponseWriter, r *http.Request) {
	id, err := uuid.Parse(r.Header.Get("X-Session-Id"))
	if err != nil {
		sh.raiseJSONError(w, err)
		return
	}

	userId, err := sh.Client.GetSessionUser(id)
	if err != nil {
		sh.raiseDBError(w, err)
		return
	}

	ttl, err := sh.Client.GetKeyExpiration(id)
	if err != nil {
		sh.raiseDBError(w, err)
		return
	}

	utils.WriteJson(w, NewResponse(map[string]interface{}{
		"user": userId,
		"ttl":  ttl.String(),
	}, nil))
}

func (sh *SessionHandler) TtlExtendSession(w http.ResponseWriter, r *http.Request) {
	id, err := uuid.Parse(r.Header.Get("X-Session-Id"))
	if err != nil {
		sh.raiseJSONError(w, err)
		return
	}

	if ok, err := sh.Client.SessionIdExist(id); err != nil {
		sh.raiseDBError(w, err)
		return
	} else if !ok {
		sh.raiseNotFound(w)
		return
	}

	if err = sh.Client.ExtendSessionTtl(id, sh.TtlDefault); err != nil {
		sh.raiseDBError(w, err)
		return
	}

	w.WriteHeader(http.StatusAccepted)
	utils.WriteJson(w, NewResponse("updated", nil))
}

func (sh *SessionHandler) DeleteSession(w http.ResponseWriter, r *http.Request) {
	id, err := uuid.Parse(r.Header.Get("X-Session-Id"))
	if err != nil {
		sh.raiseJSONError(w, err)
		return
	}

	if ok, err := sh.Client.SessionIdExist(id); err != nil {
		sh.raiseDBError(w, err)
		return
	} else if !ok {
		sh.raiseNotFound(w)
		return
	}

	if err = sh.Client.RemoveSession(id); err != nil {
		sh.raiseDBError(w, err)
		return
	}

	w.WriteHeader(http.StatusAccepted)
	utils.WriteJson(w, NewResponse("deleted", nil))
}

func (sh *SessionHandler) GetAllsession(w http.ResponseWriter, r *http.Request) {
	id, err := uuid.Parse(r.Header.Get("X-Session-Id"))
	if err != nil {
		sh.raiseJSONError(w, err)
		return
	}

	if ok, err := sh.Client.SessionIdExist(id); err != nil {
		sh.raiseDBError(w, err)
		return
	} else if !ok {
		sh.raiseNotFound(w)
		return
	}

	val, err := sh.Client.DumpSession(id)
	if err != nil {
		sh.raiseDBError(w, err)
		return
	}

	utils.WriteJson(w, NewResponse(val, nil))
}

func (sh *SessionHandler) SetSessionValue(w http.ResponseWriter, r *http.Request) {
	id, err := uuid.Parse(r.Header.Get("X-Session-Id"))
	if err != nil {
		sh.raiseJSONError(w, err)
		return
	}

	if ok, err := sh.Client.SessionIdExist(id); err != nil {
		sh.raiseDBError(w, err)
		return
	} else if !ok {
		sh.raiseNotFound(w)
		return
	}

	value := new(SessionCreate)
	if err := utils.BindJson(r, value); err != nil {
		sh.raiseJSONError(w, err)
		return
	}

	if strings.HasPrefix(value.Name, "_") {
		w.WriteHeader(http.StatusBadRequest)
		utils.WriteJson(w, NewErrorResponse(&ApiError{
			Name:        "ValidationError",
			Description: "The key cant start with '_'",
		}))
		return
	} else if value.Name == "" {
		w.WriteHeader(http.StatusBadRequest)
		utils.WriteJson(w, NewErrorResponse(&ApiError{
			Name:        "ValidationError",
			Description: "The key cant be empty",
		}))
		return
	}

	if err := sh.Client.SetValue(id, value.Name, value.Value); err != nil {
		sh.raiseDBError(w, err)
		return
	}

	w.WriteHeader(http.StatusAccepted)
	utils.WriteJson(w, NewResponse("setted", nil))
}

func (sh *SessionHandler) SetSessionValues(w http.ResponseWriter, r *http.Request) {
	id, err := uuid.Parse(r.Header.Get("X-Session-Id"))
	if err != nil {
		sh.raiseJSONError(w, err)
		return
	}

	if ok, err := sh.Client.SessionIdExist(id); err != nil {
		sh.raiseDBError(w, err)
		return
	} else if !ok {
		sh.raiseNotFound(w)
		return
	}

	value := make([]SessionCreate, 0)
	if err := utils.BindJson(r, &value); err != nil {
		sh.raiseJSONError(w, err)
		return
	}
	for _, val := range value {
		if strings.HasPrefix(val.Name, "_") {
			w.WriteHeader(http.StatusBadRequest)
			utils.WriteJson(w, NewErrorResponse(&ApiError{
				Name:        "ValidationError",
				Description: "The key cant start with '_'",
			}))
			return
		} else if val.Name == "" {
			w.WriteHeader(http.StatusBadRequest)
			utils.WriteJson(w, NewErrorResponse(&ApiError{
				Name:        "ValidationError",
				Description: "The key cant be empty",
			}))
			return
		}

		if err := sh.Client.SetValue(id, val.Name, val.Value); err != nil {
			sh.raiseDBError(w, err)
			return
		}
	}

	w.WriteHeader(http.StatusAccepted)
	utils.WriteJson(w, NewResponse("setted", nil))
}

func (sh *SessionHandler) GetAValue(w http.ResponseWriter, r *http.Request) {
	id, err := uuid.Parse(r.Header.Get("X-Session-Id"))
	key := mux.Vars(r)["key"]
	if err != nil {
		sh.raiseJSONError(w, err)
		return
	}

	val, err := sh.Client.GetSessionKey(id, key)
	if err != nil {
		if errors.Is(err, db.KeyNotFound) {
			w.WriteHeader(http.StatusNotFound)
			utils.WriteJson(w, NewErrorResponse(&ApiError{
				Name:        "NotFound",
				Description: err.Error(),
			}))
			return
		}

		sh.raiseDBError(w, err)
		return
	}

	utils.WriteJson(w, NewResponse(val, nil))
}

func (sh *SessionHandler) DeleteValue(w http.ResponseWriter, r *http.Request) {
	id, err := uuid.Parse(r.Header.Get("X-Session-Id"))
	key := mux.Vars(r)["key"]
	if err != nil {
		sh.raiseJSONError(w, err)
		return
	}

	if strings.HasPrefix(key, "_") {
		w.WriteHeader(http.StatusBadRequest)
		utils.WriteJson(w, NewErrorResponse(&ApiError{
			Name:        "ValidationError",
			Description: "The key cant start with '_'",
		}))
		return
	}

	if err := sh.Client.DelSessionKey(id, key); err != nil {
		if errors.Is(err, db.KeyNotFound) {
			w.WriteHeader(http.StatusNotFound)
			utils.WriteJson(w, NewErrorResponse(&ApiError{
				Name:        "NotFound",
				Description: err.Error(),
			}))
			return
		}

		sh.raiseDBError(w, err)
		return
	}

	w.WriteHeader(http.StatusAccepted)
	utils.WriteJson(w, NewResponse("deleted", nil))
}
