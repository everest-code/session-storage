package core

import "fmt"

type (
	ApiError struct {
		Name        string      `json:"name"`
		Description interface{} `json:"description,omitempty"`
	}

	ApiResponse struct {
		Ok      bool        `json:"ok"`
		Payload interface{} `json:"payload"`
		Error   *ApiError   `json:"error,omitempty"`
	}

	SessionCreate struct {
		Name  string      `json:"key"`
		Value interface{} `json:"value"`
	}
)

func FromError(err error) *ApiError {
	return &ApiError{
		Name:        "RuntimeError",
		Description: err.Error(),
	}
}

func NewResponse(value interface{}, err *ApiError) *ApiResponse {
	return &ApiResponse{
		Ok:      err == nil,
		Payload: value,
		Error:   err,
	}
}

func NewErrorResponse(err *ApiError) *ApiResponse {
	return &ApiResponse{
		Ok:      false,
		Payload: nil,
		Error:   err,
	}
}

func (err *ApiError) Error() string {
	if err.Description == nil {
		return err.Name
	}

	return fmt.Sprintf("%s(%s)", err.Name, err.Description)
}
