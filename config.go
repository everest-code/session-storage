package main

import (
	"os"
	"time"

	"github.com/akamensky/argparse"
	"gitlab.com/everest-code/tree-db/server/log"
)

type ConfigArgs struct {
	Expose   *string
	LogLevel *string

	RedisDB       *int
	RedisHost     *string
	RedisPassword *string

	RawTTL *string
}

func (conf *ConfigArgs) GetLogLevel() log.Level {
	switch *conf.LogLevel {
	case "DEBG":
		return log.LevelDebg
	case "INFO":
		return log.LevelInfo
	case "WARN":
		return log.LevelWarn
	case "ERRO":
		fallthrough
	default:
		return log.LevelErro
	}
}

func (conf *ConfigArgs) GetTTL() (time.Duration, error) {
	return time.ParseDuration(*conf.RawTTL)
}

func parseArgs() (args ConfigArgs) {
	parser := argparse.NewParser("session-storage", "Universal & simple session handler with redis")

	args.Expose = parser.String("p", "expose", &argparse.Options{
		Default:  ":9000",
		Help:     "[host]:port to expose the database",
		Required: false,
	})

	args.LogLevel = parser.Selector("v", "verbosity", []string{
		"DEBG",
		"INFO",
		"WARN",
		"ERRO",
	}, &argparse.Options{
		Default:  "WARN",
		Help:     "Log level for debugging and useful data",
		Required: false,
	})

	args.RedisHost = parser.String("r", "redis-host", &argparse.Options{
		Default:  "localhost:6379",
		Help:     "host:port to connect the database",
		Required: false,
	})

	args.RawTTL = parser.String("t", "ttl", &argparse.Options{
		Default:  "8h",
		Help:     "time to live for a session",
		Required: false,
	})

	args.RedisDB = parser.Int("d", "redis-db", &argparse.Options{
		Default:  0,
		Help:     "db to connect the database",
		Required: false,
	})

	args.RedisPassword = parser.String("w", "redis-password", &argparse.Options{
		Help:     "password to connect the database",
		Required: false,
	})

	err := parser.Parse(os.Args)
	if err != nil {
		os.Stdout.Write([]byte(parser.Usage(err)))
		os.Exit(1)
	}

	return
}
