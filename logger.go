package main

import (
	"net/http"
	"time"
)

type loggingResponseWriter struct {
	http.ResponseWriter
	statusCode int
	bodyLength int
}

func newLoggingResponseWriter(w http.ResponseWriter) *loggingResponseWriter {
	// WriteHeader(int) is not called if our response implicitly returns 200 OK, so
	// we default to that status code.
	return &loggingResponseWriter{w, http.StatusOK, 0}
}

func (lrw *loggingResponseWriter) WriteHeader(code int) {
	lrw.statusCode = code
	lrw.ResponseWriter.WriteHeader(code)
}

func (lrw *loggingResponseWriter) Write(body []byte) (int, error) {
	length, err := lrw.ResponseWriter.Write(body)
	lrw.bodyLength += length
	return length, err
}

func headerMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Add("Server", "SessionStorage/v1.0")

		w.Header().Add("Access-Control-Allow-Origin", "*")
		w.Header().Add("Access-Control-Allow-Headers", "Content-Type, X-Session-Id")
		w.Header().Add("Access-Control-Expose-Headers", "Content-Type, Content-Length, Server")
		w.Header().Add("Access-Control-Allow-Methods", "GET,OPTIONS,POST,PUT,DELETE")
		next.ServeHTTP(w, r)
	})

}

func loggerMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		start := time.Now()
		lrw := newLoggingResponseWriter(w)
		next.ServeHTTP(lrw, r)
		diff := time.Now().Sub(start)

		handler := Logger.Debug
		if 300 <= lrw.statusCode && lrw.statusCode <= 399 {
			handler = Logger.Info
		} else if 400 <= lrw.statusCode && lrw.statusCode <= 499 {
			handler = Logger.Warn
		} else if 500 <= lrw.statusCode && lrw.statusCode <= 599 {
			handler = Logger.Error
		}

		handler("%s %s -> %d %s %db", r.Method, r.URL.Path, lrw.statusCode, diff, lrw.bodyLength)
	})

}
