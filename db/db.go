package db

import (
	"context"
	"errors"
	"fmt"
	"strconv"
	"time"

	"github.com/go-redis/redis/v8"
	"github.com/google/uuid"
	"gitlab.com/everest-code/session-storage/utils"
	"gitlab.com/everest-code/tree-db/server/log"
)

type SessionDatabase struct {
	*redis.Client
	Logger *log.Logger
}

var (
	ctx               = context.Background()
	AlreadyExistsUser = errors.New("The user already has a session")
	UserNotFound      = errors.New("The user hasn't session yet")
	KeyNotFound       = errors.New("The key was not found")
)

func CreateConnection(host string, db int, password string, logger *log.Logger) (*SessionDatabase, error) {
	rdb := redis.NewClient(&redis.Options{
		Addr:     host,
		DB:       db,
		Password: password,
	})

	err := rdb.Ping(ctx).Err()
	return &SessionDatabase{rdb, logger}, err
}

func (sd *SessionDatabase) SessionExist(user int) (bool, error) {
	userStr := fmt.Sprintf("%d", user)
	lastSession, err := sd.HGet(ctx, "sessions", userStr).Result()
	if err != nil {
		if err.Error() == "redis: nil" {
			return false, nil
		}
		return false, err
	}

	redisKey := fmt.Sprintf("sessions:%s", lastSession)
	keyCount, err := sd.Exists(ctx, redisKey).Result()
	return keyCount == 1, err
}

func (sd *SessionDatabase) SessionIdExist(id uuid.UUID) (bool, error) {
	redisKey := fmt.Sprintf("sessions:%s", id.String())
	keyCount, err := sd.Exists(ctx, redisKey).Result()
	return keyCount == 1, err
}

func (sd *SessionDatabase) GetUserID(user int) (id uuid.UUID, err error) {
	userStr := fmt.Sprintf("%d", user)
	exist, err := sd.SessionExist(user)
	if err != nil {
		return
	} else if !exist {
		err = UserNotFound
		return
	}

	sessionId, err := sd.HGet(ctx, "sessions", userStr).Result()
	if err != nil {
		return
	}

	return uuid.Parse(sessionId)
}
func (sd *SessionDatabase) GetSessionUser(id uuid.UUID) (user int, err error) {
	sessionsRaw, err := sd.HGetAll(ctx, "sessions").Result()
	session := make(map[string]int)
	for k, v := range sessionsRaw {
		session[v], _ = strconv.Atoi(k)
	}
	user, ok := session[id.String()]
	if !ok {
		err = UserNotFound
	}

	return
}

func (sd *SessionDatabase) CreateEmpty(user int, ttl time.Duration) (id uuid.UUID, err error) {
	id, userStr := uuid.New(), fmt.Sprintf("%d", user)
	lastSession, err := sd.HGet(ctx, "sessions", userStr).Result()
	if err != nil && err.Error() != "redis: nil" {
		return
	} else if err == nil && lastSession != "" {
		redisKey := fmt.Sprintf("sessions:%s", lastSession)
		keyCount, err := sd.Exists(ctx, redisKey).Result()
		if err != nil {
			return id, err
		}

		if keyCount != 0 {
			return id, AlreadyExistsUser
		}
	}

	err = sd.HSet(ctx, "sessions", userStr, id.String()).Err()
	if err != nil {
		return
	}

	sessionKey := fmt.Sprintf("sessions:%s", id.String())

	err = sd.HSet(
		ctx,
		sessionKey,
		"_user_id",
		userStr,
	).Err()
	if err != nil {
		return
	}

	return id, sd.Expire(ctx, sessionKey, ttl).Err()
}

func (sd *SessionDatabase) ExtendSessionTtl(id uuid.UUID, ttl time.Duration) error {
	sessionKey := fmt.Sprintf("sessions:%s", id.String())
	return sd.Expire(ctx, sessionKey, ttl).Err()
}

func (sd *SessionDatabase) SetValue(id uuid.UUID, key string, value interface{}) error {
	redisKey := fmt.Sprintf("sessions:%s", id.String())

	val, err := utils.AsJson(value)
	if err != nil {
		return err
	}

	return sd.HSet(ctx, redisKey, key, val).Err()
}

func (sd *SessionDatabase) GetKeyExpiration(id uuid.UUID) (time.Duration, error) {
	redisKey := fmt.Sprintf("sessions:%s", id.String())
	return sd.TTL(ctx, redisKey).Result()
}

func (sd *SessionDatabase) RemoveSession(id uuid.UUID) error {
	redisKey := fmt.Sprintf("sessions:%s", id.String())
	if err := sd.Del(ctx, redisKey).Err(); err != nil {
		return err
	}

	userId, err := sd.GetSessionUser(id)
	if err != nil {
		return err
	}

	return sd.HDel(ctx, "sessions", fmt.Sprintf("%d", userId)).Err()
}

func (sd *SessionDatabase) DumpSession(id uuid.UUID) (map[string]interface{}, error) {
	if ok, err := sd.SessionIdExist(id); err != nil {
		return nil, err
	} else if !ok {
		return nil, UserNotFound
	}

	val, err := sd.HGetAll(ctx, fmt.Sprintf("sessions:%s", id.String())).Result()
	if err != nil {
		return nil, err
	}

	retVal := make(map[string]interface{})
	for k, v := range val {
		sv := new(interface{})
		if err := utils.FromJson(v, sv); err != nil {
			return nil, err
		}

		retVal[k] = sv
	}

	return retVal, nil
}

func (sd *SessionDatabase) GetSessionKey(id uuid.UUID, key string) (interface{}, error) {
	if ok, err := sd.SessionIdExist(id); err != nil {
		return nil, err
	} else if !ok {
		return nil, UserNotFound
	}

	val, err := sd.HGet(ctx, fmt.Sprintf("sessions:%s", id.String()), key).Result()
	if err != nil {
		if err.Error() == "redis: nil" {
			return nil, KeyNotFound
		}
		return nil, err
	}

	sv := new(interface{})
	if err := utils.FromJson(val, sv); err != nil {
		return nil, err
	}

	return sv, nil
}

func (sd *SessionDatabase) DelSessionKey(id uuid.UUID, key string) error {
	if ok, err := sd.SessionIdExist(id); err != nil {
		return err
	} else if !ok {
		return UserNotFound
	}

	count, err := sd.HDel(ctx, fmt.Sprintf("sessions:%s", id.String()), key).Result()
	if err != nil {
		return err
	} else if count == 0 {
		return KeyNotFound
	}

	return nil
}
