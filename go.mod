module gitlab.com/everest-code/session-storage

go 1.16

require (
	github.com/akamensky/argparse v1.3.1
	github.com/go-redis/redis/v8 v8.11.4
	github.com/google/uuid v1.3.0
	github.com/gorilla/mux v1.8.0
	gitlab.com/everest-code/tree-db/server v0.1.1
)
